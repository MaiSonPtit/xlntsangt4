# Lien tuc
import numpy as np                      #khai bao pip numpy de su dung cac ham trong day
import matplotlib.pyplot as plt         #khai bao pip matplotlid de co the ve do thi
A = .8                                  #Cho bien do la 0.8
f = 5                                   #Chon tan so la 5Hz
t = np.arange(0,1,.01)                  #Khai bao mang t co gia tri bat dau la 0 ket thuc la 0.99 buoc nhay 0.01
phi = np.pi/4                           #Goc phi bang 180/4
x = A*np.cos(2*np.pi*f*t + phi)         #khai bao phuong trinh dao dong voi x la li do
plt.plot(t,x)                           #Ham ve do thi dua tren hai gia tri la t va x
plt.axis([0,1,-1,1])                    #Gia tri cua hai truc: bien do tu -1 den 1 va truc thoi gian tu 0 den 1
plt.xlabel('time in seconds')           #Ham in chu thich cho truc x o day la bien do
plt.ylabel('amplitude')                 #Ham in chu thich cho truc y o day la thoi gian
plt.show()                              #Ham show do thi tren man hinh
#-----------------------------------------------------------------------------------------------------------------------
# Roi rac
A = .65                                 #Bien do giao dong 0.65
fs = 100                                #Tan so mau 100
samples = 100                           #Tao mau bang 100
f = 5                                   #Tan so dao dong 5Hz
phi = 0                                 #Cho goc phi ban dau tai vi tri 0
n = np.arange(samples)                  #Tao mot mang so luong mau  n co gia tri mac dinh ban dau la khong gia tri ket thuc 99 buoc nhay la 1
T = 1.0/fs                              #Remember to use 1.0 and not 1 ep kieu float!/ tinh thoi gian lay mau
y = A*np.cos(2*np.pi*f*n*T + phi)       #Phuong trinh dao dong li do y tan so 5Hz va goc phi la 0
plt.plot(y)                             #Ve phuong trinh dao dong y
plt.axis([0,100,-1,1])                  #Ve khung voi truc x tu 0 den 100 va truc y tu -1 den 1
plt.xlabel('sample index')              #Ham in chu thich cho truc x o day la bien do
plt.ylabel('amplitude')                 #Ham in chu thich cho truc y o day la thoi gian
plt.show()                              #Ham show do thi tren man hinh
#-----------------------------------------------------------------------------------------------------------------------
# Roi Rac
A = .8                                  #Bien do 0.8
N = 100 # samples                       #So luong mau
f = 5                                   #Tan so 5Hz
phi = 0                                 #Goc phi bang 0
n = np.arange(N)                        #Tao mang bat dau tu 0 ket thuc 99 buoc nhay 1
y = A*np.cos(2*np.pi*f*n/N + phi)       #Phuong trinh lay mau
plt.plot(y)                             #Ve phuong trinh lay mau
plt.axis([0,100,-1,1])                  #Ve khung
plt.xlabel('sample index')              #Ham in chu thich cho truc x o day la bien do
plt.ylabel('amplitude')                 #Ham in chu thich cho truc y o day la thoi gian
plt.show()                              #Ham show do thi tren man hinh
#-----------------------------------------------------------------------------------------------------------------------
f = 3                                   #Tan so 3Hz
t = np.arange(0,1,.01)                  #Mang t bat dau tu 0 ket thuc 0.99 buoc nhay 0.01
phi = 0                                 #Pha ban dau bang 0
x = np.exp(1j*(2*np.pi*f*t + phi))      #x la phuong trinh ej(2πft+ϕ)
xim = np.imag(x)                        #Xim la lay phan ao cau bieu thuc x
plt.figure(1)                           #Ta se tao mot cua so trong ten figure(1)
plt.plot(t,np.real(x))                  #Ve do thi dua vao thoi gia va phan thuc cua bieu thuc x
plt.plot(t,xim)                         #Ve do thi dua vao thoi gia va ao cua bieu thuc x
plt.axis([0,1,-1.1,1.1])                #Tao khung khung voi x tu 0 den 1 va y tu -1.1 den 1.1
plt.xlabel('time in seconds')           #Ham in chu thich cho truc x
plt.ylabel('amplitude')                 #Ham in chu thich cho truc y
plt.show()                              #Ham show do thi tren man hinh
#-----------------------------------------------------------------------------------------------------------------------
# Roi rac
f = 3                                   #Tan so 3Hz
N = 100                                 #Tao bien N bang 100
fs = 100                                #Tan so mau 100
n = np.arange(N)                        #Tao mang bat dau tu 0 ket thuc 99 buoc nhay 1
T = 1.0/fs                              #Thoi gian lay mau
t = N*T                                 #Thoi gian
phi = 0                                 #Pha ban dau bang 0
x = np.exp(1j*(2*np.pi*f*n*T + phi))    #Phuong trinh x
xim = np.imag(x)                        #Lay phan ao
plt.figure(1)                           #Tao cua so trong
plt.plot(n*T,np.real(x))                #Ve do thi dua tren phan thuc cua x va thoi gian lay mau,so luong mau
plt.plot(n*T,xim)                       #Ve do thi dua tren phan ao cua x va thoi gian lay mau,so luong mau
plt.axis([0,t,-1.1,1.1])                #Tao khung co gia tri tai x tu 0 den t va y tu -1.1 den 1.1
plt.xlabel('t(seconds)')                #Ham in chu thich cho truc x
plt.ylabel('amplitude')                 #Ham in chu thich cho truc y
plt.show()                              #Ham show do thi tren man hinh
#-----------------------------------------------------------------------------------------------------------------------
f = 3                                   #Tan so 3Hz
N = 64                                  #So mau 64
n = np.arange(64)                       #Mang n tu 0 den 63 buowc nhay 1
phi = 0                                 #pha ban dau la 0
x = np.exp(1j*(2*np.pi*f*n/N + phi))    #Phuong trinh phuc
xim = np.imag(x)                        #Lay phan ao trong x
plt.figure(1)                           #Tao cua so trong
plt.plot(n,np.real(x))                  #Ve do thi du tren mau va phan thuc cua x
plt.plot(n,xim)                         #Ve do thi du tren mau va phan ao cua x
plt.axis([0,N,-1.1,1.1])                #Truc x tu 0 den 64 truc y tu -1.1 den 1.1
plt.xlabel('sample index')              #Ham in chu thich cho truc x
plt.ylabel('amplitude')                 #Ham in chu thich cho truc y
plt.show()                              #Ham show do thi tren man hinh
#-----------------------------------------------------------------------------------------------------------------------
# ????                                  
#N = 44100                              # samples
#f = 440                                #Tan so 440Hz
#fs = 44100                             #Tan so mau 44100
#phi = 0                                #Phan ban dau bang 0
#n = np.arange(N)                       #Tao mang tu 0 den 44099 bươc nhay 1
#x = A*np.cos(2*np.pi*f*n/N + phi)      #Phuong trinh x 
#scipy.io.wavfile.write(filename, rate, data)[source]
#from scipy.io.wavfile import write
#write('sine440_1sec.wav', 44100, x)    #Doc ghi file vao file ten sine440_1sec.wav